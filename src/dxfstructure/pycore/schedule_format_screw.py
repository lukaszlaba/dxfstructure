'''
--------------------------------------------------------------------------
Copyright (C) 2017-2018 Lukasz Laba <lukaszlab@o2.pl>

This file is part of DxfStructure (structural engineering dxf drawing system).

DxfStructure is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

DxfStructure is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
--------------------------------------------------------------------------
'''

from tabulate import tabulate

language = 'PL'

def get_available_languages():
    return ['PL', 'EN']

def set_language(newlanguage='PL'):
    global language
    if newlanguage in get_available_languages():
        language = newlanguage

def title():
    if language == 'EN':
        title =      'BOLTS SCHEDULE'
    if language == 'PL':
        title =      'ZESTAWIENIE SRUB'
    return title


def header():
    if language == 'EN':
        header_1 =      ['Element',  'Bolt',    'Length',   'Grade',  'Number',     'Number of',    'Total']
        header_2 =      [  '  ',     'type',     '  ',       ' ',       'of',         'bolts',      'number']
        header_3 =      [  '  ',      '  ',      '[mm]',      ' ',    'elements',   'in element',     '  ']
    if language == 'PL':
        header_1 =      ['Element',   'Typ',     'Dlugosc',   'Klasa',  'Ilosc',   'Ilosc srub',    'Laczna']
        header_2 =      [  '  ',     'sruby',     'sruby',    'sruby',  'elementow',   'w elemencie',  'ilosc']
        header_3 =      [  '  ',      '  ',      '[mm]',      ' ',      '   ',           '  ',         '  ']
    return [header_1, header_2, header_3]

def breake_mark():
    return [len(header()[0]) * ['---']]

def record(screw):
    #---
    Member = screw.element.name
    #---
    Screwtype = screw.Screwtype
    #---
    Length = screw.Length
    if not Length: Length = '-'
    #---
    Grade = screw.Grade
    if not Grade: Grade = '-'
    #---
    Number_of_members = screw.element.quantity
    #---
    Number_of_profiles_in_element = screw.Number
    #---
    Total_number = screw.Number * screw.element.quantity
    #---
    record =[ Member, Screwtype, Length, Grade, Number_of_members, Number_of_profiles_in_element, Total_number]
    return [record]

# Test if main
if __name__ == "__main__":
    from dxfstructure.pycore.environment import*
    DRAWING.open_file()
    SCANER.load_data_to_model()
    print(tabulate(header(), numalign="right"))
    screw = STEEL_MODEL.screwlist[2]
    print(tabulate(record(screw), numalign="right"))
    #print (tabulate(header() + record(profile), numalign="right"))