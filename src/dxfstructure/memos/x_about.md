##*DxfStructure 0.4 (beta version for testing)*


Structural engineering dxf drawing system

(c) 2017-2022 Łukasz Laba (e-mail : lukaszlaba@gmail.com)


##*Other information*
Project websites:

*bitbucket.org/lukaszlaba/dxfstructure/wiki/Home*


E-mail : *lukaszlaba@gmail.com*