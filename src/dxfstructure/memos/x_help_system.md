##*DxfStructure system help*

---
###*Available functions*

|Name|Description|
|-------|----------|
|`(1)Inject DS system to file`| Creates DS system (layers etc.) in current dxf file|
|`(2)Set language`| It sets language for schedules inserted in drawing|
---